# -*- coding: UTF-8 -*-
from flask import Flask, Response, render_template
import time

app = Flask(__name__)

#이벤트 스트림을 출력하는 제네레이터 함수.
def event_stream():
    for message in xrange(100):
        # 아래의 sleep은 하지 않아도 무방
        time.sleep(1)
        # SSE의 기본 데이터 프로토콜은 다음과 같다. 다른 형식으로 출력하려면 아래의 하이퍼링크를 참조
        # http://www.html5rocks.com/en/tutorials/eventsource/basics/?redirect_from_locale=ko
        yield 'data: %d\n\n' % message

@app.route('/', methods=['GET'])
def index():
    #render_template함수는 unicode문자열 리턴
    return render_template('index.html')

@app.route('/stream')
def stream():
    #mimetype이 text/event-stream인 파일을 전송하면 SSE성립
    return Response(event_stream(), mimetype="text/event-stream")

if __name__ == '__main__':
    #플래스크 어플리케이션을 쓰레드화 하지 않으면 1개 이상 클라이언트에 대응이 불가능하다.
    app.run(debug=True, threaded=True)